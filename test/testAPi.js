const assert = require('assert');
const fetch = require('node-fetch');
// describe('Probando API', () => {
//     it('Api responde estado 200', async () => {
        
//         await fetch('https://jsonplaceholder.typicode.com/todos/1')
//         .then((response) =>{
//             console.log('status',response.status);
//             //como si emularamos ===
//             assert.strictEqual(response.status,200);
//         });
//     });

//     it('Api responde estado 404', async () => {
//         await fetch('https://jsonplaceholder.typicode.com/todos/1xyse')
//         .then((response) =>{
//             console.log('status',response.status);
//             assert.strictEqual(response.status,404);
//         })
//     });

//     it('El user_Id de la respuesta debe ser 1', async () => {
//         await fetch('https://jsonplaceholder.typicode.com/todos/1')
//         .then(response => response.json())
//         .then(json => {
//             console.log('data',json);
//             assert.strictEqual(json.userId,1);
//         });
//     });
// })

describe("Probando API", () => {
     
    it("API responde 200", async () => {
       await fetch('https://jsonplaceholder.typicode.com/todos/1')
       .then(response => {
           console.log(response.status)
           assert.strictEqual(response.status, 200);
       })
    });

    it("API responde 404", async () => {
       await fetch('https://jsonplaceholder.typicode.com/todos/1zxcv')
       .then(response => {
           console.log(response.status)
           assert.strictEqual(response.status, 404);
       })
    });

    it("El user_id de la respuesta debe ser 1", async () => {
       await fetch('https://jsonplaceholder.typicode.com/todos/1')
       .then(response => response.json())
       .then(json => {
           assert.strictEqual(json.userId, 1);
       })
    });
 
});
